import { ListaAutos } from "./componets/autos/ListaAutos";
import { Provider } from "react-redux";
import { store } from "./store/store";

function App() {
  return (
    <Provider store={store}>
      <ListaAutos />
    </Provider>
  );
}

export default App;
