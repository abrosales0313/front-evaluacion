import { AutoType } from "../types/AutoType";
import { types } from "../types/types";


const initialState:any ={
    listaAutos: []
}

export const autosReducer =(state = initialState, action: any) =>{

    switch (action.type) {
        case types.eventSetAutos:
            return{
                ...state,
                listaAutos: action.payload
        }
        case types.eventAutoUpdated:
            return{
                ...state,
                listaAutos: state.listaAutos.map(
                    (a: AutoType) => (a.id === action.payload.id)? action.payload: a
                )
            }
        default:
            return state
    }

};