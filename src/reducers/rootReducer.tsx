import { combineReducers } from "redux";
import { autosReducer } from "./autosReducer";
import { mantenimientoReducer } from "./manteniminetoReducer";
import { uiReducer } from "./uiReducer";

export const rootReducer = combineReducers({
    ui: uiReducer,
    autos: autosReducer,
    mantenimiento: mantenimientoReducer
})