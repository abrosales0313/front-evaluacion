import { types } from "../types/types";


const initialState:any ={ }

export const mantenimientoReducer =(state = initialState, action: any) =>{

    switch (action.type) {
        case types.eventSetMantenimiento:
            return{
                ...state,
                ...action.payload
            }
        default:
            return state
    }

};