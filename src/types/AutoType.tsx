export type AutoType = {
    description: String,
    make: String,
    model: String,
    estimatedate?: String,
    id: number,
    image?: string,
    km?: Number
    enMantenimiento?: boolean
}

