const baseUrl = 'https://evaluacion-trabajo.herokuapp.com/autos'

const fetchObtenerAutos = ()=>{
    return fetch(baseUrl);
}

const fetchGuardarMantenimiento = (payload: any)=>{
    return fetch(`${baseUrl}/${payload.idAuto}/mantenimiento`,{ 
        method: 'PUT',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(payload)
    });
}

export{
    fetchObtenerAutos,
    fetchGuardarMantenimiento
}