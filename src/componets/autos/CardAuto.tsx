import { useDispatch } from "react-redux"
import { eventSetMantenimiento } from "../../actions/autosEvents";
import { uiOpenModal } from "../../actions/ui"
import { AutoType } from "../../types/AutoType"


const CardAuto = (auto: AutoType) => {
    const dispatch = useDispatch();
    const abrirModal = ()=>{

        if(!auto.enMantenimiento){
            dispatch(eventSetMantenimiento({
                idAuto: auto.id
            }));
            dispatch(uiOpenModal());
        }
    }
    
    return (
        <div className="px-1 mb-3 col-xs-12 col-sm-6 col-md-4 cardAuto" aria-disabled={auto.enMantenimiento} onClick={abrirModal}>
            <div className="card shadow">
                <div className="row g-0">
                    <div className="col-md-4">
                        <img className="img-fluid" src={auto?.image} alt="..." />
                    </div>
                    <div className="col-md-8">
                        <div className="card-body">
                            <h5 className="card-title">{auto?.make} {auto?.model}</h5>
                            <p className="card-text"> {auto?.description}</p>
                            <p className="card-text"><small className="text-muted">{auto?.estimatedate}</small></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>        
    )
}

export default CardAuto
