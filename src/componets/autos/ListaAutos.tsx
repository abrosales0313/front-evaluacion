import { useEffect } from 'react'
import { AutoType } from "../../types/AutoType";
import CardAuto from "./CardAuto";
import { Navbar } from "../ui/Navbar"
import {fetchObtenerAutos} from '../../helpers/fetch'
import ModalInfoManteiner from '../modal/ModalInfoManteiner';
import { useDispatch, useSelector } from 'react-redux';
import { eventSetAutos } from '../../actions/autosEvents';

export const ListaAutos = () => {
    
    //const [autos, setAutos] = useState<Array<AutoType>>([]);
    const {listaAutos} = useSelector<any,any>(state => state.autos);
    const dispatch = useDispatch();
    
    const datos = async() =>{
        try {
            const d = await fetchObtenerAutos()
            const info = await d.json()                        
            dispatch(eventSetAutos([...info]))
        } catch (error) {
            console.log(error);            
        }    
    }
    useEffect(() => {
        datos()
    },[])
    
    return (
        <div className="mt-5">
            <Navbar />
            <div className="container pt-5">
                <div className="row">
                    { listaAutos.map((auto: AutoType) => {
                        return <CardAuto key={auto.id} {...auto} />
                    })}
                </div>                
            </div>
            <ModalInfoManteiner />
        </div>
    )
}
