import { useState } from 'react';
import Modal from 'react-modal';
import { useSelector, useDispatch } from 'react-redux';
import { eventAutoUpdated } from '../../actions/autosEvents';
import { uiCloseModal } from '../../actions/ui';
import { fetchGuardarMantenimiento } from '../../helpers/fetch';
import { AutoType } from '../../types/AutoType';

const customStyles = {
  content: {
    top: '50%',
    left: '50%',
    right: 'auto',
    bottom: 'auto',
    marginRight: '-50%',
    transform: 'translate(-50%, -50%)'
  }
};
Modal.setAppElement('#root')
function ModalInfoManteiner() {

  const [formValues, setFormValues] = useState({});
  const { modalOpen } = useSelector<any, any>(state => state.ui);
  const {idAuto} = useSelector<any, any>(state => state.mantenimiento);
  const {listaAutos} = useSelector<any,any>(state => state.autos);
  const dispatch = useDispatch();

  const onInputChage = ({target}: any) => {
    setFormValues({
      ...formValues,
      [target.name]: target.value
    });
  };

  const cerrarModal = () => {
    dispatch(uiCloseModal())
  }

  const guardarMantenimiento = async() => {
    try {
      await fetchGuardarMantenimiento({
        idAuto,
        ...formValues,
      });
      const auto = listaAutos.find((a:AutoType) => a.id === idAuto);      
      dispatch(eventAutoUpdated({...auto, enMantenimiento: true}));
      cerrarModal();
    } catch (error) {
      console.log(error);
      
    }
  }

  
  return (
    <Modal
      isOpen={modalOpen}
      style={customStyles}
      onRequestClose={cerrarModal}
      closeTimeoutMS={200}
      className="modal"
      overlayClassName="modal-fondo"
    >
      <div className="mb-3">
        <label htmlFor="exampleFormControlInput1" className="form-label">Nombre</label>
        <input name="nombre" type="text" className="form-control" id="exampleFormControlInput1" placeholder="Jose Perez" onChange={onInputChage}/>
      </div>
      <div className="mb-3">
        <label htmlFor="exampleFormControlInput2" className="form-label">Fecha estimada</label>
        <input name="fechaEstimada" type="date" className="form-control" id="exampleFormControlInput2" onChange={onInputChage} />
      </div>
      <div className="text-center">
        <button type="button" className="btn btn-primary mx-2" onClick={guardarMantenimiento}>Acetar</button>
        <button type="button" className="btn btn-danger mx-2" onClick={cerrarModal}>Cancelar</button>
      </div>
    </Modal>
  )
}

export default ModalInfoManteiner
