import { types } from "../types/types";


export const eventSetAutos = (event: any)=>({
    type: types.eventSetAutos,
    payload: event
})

export const eventAutoUpdated = (event: any)=>({
    type: types.eventAutoUpdated,
    payload: event
})

export const eventSetMantenimiento = (event: any)=>({
    type: types.eventSetMantenimiento,
    payload: event
})